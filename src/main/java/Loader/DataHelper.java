package Loader;

import Validators.HttpValidator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataHelper {
    public List<String> readData(String filePath){
        List<String> result = new ArrayList<String>();
        HttpValidator httpValidator = new HttpValidator();
        try {
            FileInputStream fs = new FileInputStream(filePath);
            BufferedReader bf = new BufferedReader (new InputStreamReader(fs));
            String curLine;
            while ((curLine = bf.readLine())!= null){
                if (httpValidator.httpValid(curLine)){
                    result.add(curLine);
                } else {
                    System.out.println("Not valid string '" + curLine + "'");
                    System.out.println("Only HTTP URL support.");
                }
            }
        } catch (IOException e) {
            System.out.println("Can't read file on path '"+ filePath +"'");
        }
        return result;
    }

    public void writeResult(String pathOfFile, List<String> data) {
        if (data.size() == 0) return;
        try {
            FileWriter writer = new FileWriter(pathOfFile, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            for(String str : data){
                bufferWriter.write(str + "\n");
            }
            bufferWriter.write("End of url" + "\n");
            bufferWriter.close();
        } catch (IOException e) {
            System.out.println("Can't write data on file '"+ pathOfFile +"'");
        }
    }
}
