package Loader;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import java.util.List;

public class Main {
    @Parameter(names = "-s", description = "Source url list", required = true)
    public String targetList;
    @Parameter(names = "-r", description = "Result", required = true)
    public String targetFile;
    public static void main(String[] args) {
        Main main = new Main();
        JCommander jCommander = new JCommander(main);
        try {
            jCommander.parse(args);
        } catch (ParameterException ex){
            jCommander.usage();
            return;
        }
        main.run();
    }

    private void run() {
        List<String> urlList;
        DataHelper dataHelper = new DataHelper();
        HttpHelper httpHelper = new HttpHelper();
        // Get urls
        urlList = dataHelper.readData(targetList);
        if (urlList.size() == 0){
            System.out.println("Empty or wrong data on file '" + targetList + "'");
            System.out.println("Done!");
            return;
        }
        for (String s : urlList){
            List<String> data;
            data = httpHelper.getDataFromUrl(s);
            dataHelper.writeResult(targetFile, data);
        }
        System.out.println("Done!");
    }
}
