package Loader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpHelper {
    public List<String> getDataFromUrl(String s){
        String line;
        List<String> respStrings = new ArrayList<String>();

        HttpClient client = new DefaultHttpClient();
        try {
            HttpGet request = new HttpGet(s);
            HttpResponse response;
            try {
                response = client.execute(request);
                BufferedReader bf = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
                while ((line = bf.readLine()) != null) {
                    respStrings.add(line);
                }
            } catch (IOException e) {
                System.out.println("Can't get data from '" + s + "'");
            }
        } catch (IllegalArgumentException ex){
            System.out.println("Can't get data from '" + s + "' "+ "Invalid character.");
            return respStrings;
        }


        return respStrings;
    }
}
