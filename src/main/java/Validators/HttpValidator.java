package Validators;

public class HttpValidator {
    public boolean httpValid(String s){
        if (s.length() > 8){
            if (s.substring(0, 8).equals("https://")){
                return true;
            }else {
                if (s.length() > 7){
                    if (s.substring(0, 7).equals("http://")){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
