package UrlLoader;

import Validators.HttpValidator;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ValidatorTests {
    @Test
    public void httpTest(){
        HttpValidator httpValidator = new HttpValidator();
        assertTrue(httpValidator.httpValid("http://qwe"));
    }

    @Test
    public void httpsTest(){
        HttpValidator httpValidator = new HttpValidator();
        assertTrue(httpValidator.httpValid("https://qwe"));
    }

    @Test
    public void notHttpString(){
        HttpValidator httpValidator = new HttpValidator();
        assertTrue(! httpValidator.httpValid("http2s://qwe"));
    }
}
